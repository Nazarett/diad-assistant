//
//  ListUser.swift
//  GoogleToolboxForMac
//
//  Created by Gerardo Nazarett on 10/30/18.
//

import Foundation

class ListUser{
    let name : String
    let doh : String?
    let uid : String
    
    init(name : String, doh : String?,
         uid: String){
        self.name = name
        self.doh = doh
        self.uid = uid
    }
}
