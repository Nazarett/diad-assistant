//
//  GetEdd.swift
//  DIADAssistant
//
//  Created by Gerardo Nazarett on 10/29/18.
//  Copyright © 2018 Gerardo Nazarett. All rights reserved.
//

import UIKit

class GetEdd: UIViewController {
    
    //let backgroundImage = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setBackground()
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    //    func setBackground(){
    //        view.addSubview(backgroundImage)
    //        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
    //        backgroundImage.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    //        backgroundImage.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    //        backgroundImage.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
    //        backgroundImage.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    //
    //        backgroundImage.image = UIImage(named: "GetEDD")
    //        view.sendSubviewToBack(backgroundImage)
    //
    //    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
