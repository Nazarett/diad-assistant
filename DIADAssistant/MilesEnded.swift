//
//  MilesEnded.swift
//  DIADAssistant
//
//  Created by Gerardo Nazarett on 10/30/18.
//  Copyright © 2018 Gerardo Nazarett. All rights reserved.
//

import UIKit

class MilesEnded: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var endedMiles: UITextField!
    
    
    @IBAction func nextButton(_ sender: Any) {
        if Int(endedMiles.text!) == 0{
            let alert = UIAlertController(title: "Alert", message: "Miles must be greater than zero", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        if endedMiles.text == ""{
            let alert = UIAlertController(title: "Alert", message: "Please Fill In Miles", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        endedMiles.delegate = self
        // Do any additional setup after loading the view.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "passFinishMiles"{
            if let dvc = segue.destination as? MilesEndedTwo{
                dvc.passFinishMiles = self.endedMiles.text
            }
        }
    }
    
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var startString = ""
        if (endedMiles.text != nil){
            startString += endedMiles.text!
        }
        startString += string
        
        let limitNumber = startString.count
        if limitNumber > 6{
            return false
        }else{
            return true
        }
    }
}
