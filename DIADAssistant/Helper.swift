//
//  Helper.swift
//  DIADAssistant
//
//  Created by Gerardo Nazarett on 10/29/18.
//  Copyright © 2018 Gerardo Nazarett. All rights reserved.
//


import UIKit
import Firebase

class Helper: UIViewController {
    
    let backgroundImage = UIImageView()
    
    @IBAction func toProfile(_ sender: Any) {
        self.performSegue(withIdentifier: "helperToProfile", sender: nil)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackground()
        // Do any additional setup after loading the view.
    }
    
    
    func setBackground(){
        view.addSubview(backgroundImage)
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        backgroundImage.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImage.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundImage.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundImage.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        backgroundImage.image = UIImage(named: "HelperPagenew")
        view.sendSubviewToBack(backgroundImage)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "helperToProfile"{
            if let uid = Auth.auth().currentUser?.uid{
                if let dvc = segue.destination as? Profile{
                    dvc.uid = uid
                }
            }
        }
    }
    
}
