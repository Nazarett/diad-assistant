//
//  Timecardinfo.swift
//  DIADAssistant
//
//  Created by Gerardo Nazarett on 10/30/18.
//  Copyright © 2018 Gerardo Nazarett. All rights reserved.
//

import Foundation

class TimecardInfo{
    let punch : String
    let date : String
    let time : String
    
    
    init(punch: String, date : String, time : String){
        self.punch = punch
        self.date = date
        self.time = time
    }
}
