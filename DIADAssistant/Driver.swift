//
//  Driver.swift
//  DIADAssistant
//
//  Created by Gerardo Nazarett on 9/18/18.
//  Copyright © 2018 Gerardo Nazarett. All rights reserved.
//

import UIKit
import Firebase

class Driver: UIViewController {
    
    let backgroundImage = UIImageView()
    
    
    @IBAction func toProfile(_ sender: Any) {
        self.performSegue(withIdentifier: "driverToProfile", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setBackground()
        //self.navigationController?.navigationBar.isHidden = false
        // Do any additional setup after loading the view.
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "driverToProfile"{
            if let uid = Auth.auth().currentUser?.uid{
                if let dvc = segue.destination as? Profile{
                    dvc.uid = uid
                }
            }
        }
    }
    
    
    func setBackground(){
        view.addSubview(backgroundImage)
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        backgroundImage.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImage.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundImage.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundImage.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        backgroundImage.image = UIImage(named: "DriverPagenew")
        view.sendSubviewToBack(backgroundImage)
        
    }
    
    
    
}
