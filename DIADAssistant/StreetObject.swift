//
//  StreetObject.swift
//  DIADAssistant
//
//  Created by Gerardo Nazarett on 3/29/19.
//  Copyright © 2019 Gerardo Nazarett. All rights reserved.
//

import Foundation

class StreetAddress{
    
    let streetNumber : String
    let streetName : String
    let direction : String
    let city : String
    let zipCode : String
    let room : String
    let floor : String
    
    init(stNumber: String, stName: String, stDirection: String, stCity: String, stZip: String, stRoom: String, stFloor: String ){
        streetNumber = stNumber
        streetName = stName
        direction = stDirection
        city = stCity
        zipCode = stZip
        room = stRoom
        floor = stFloor
        
    }
    
}
