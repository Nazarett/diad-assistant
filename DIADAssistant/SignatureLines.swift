//
//  SignatureLines.swift
//  DIADAssistant
//
//  Created by Gerardo Nazarett on 10/29/18.
//  Copyright © 2018 Gerardo Nazarett. All rights reserved.
//

import Foundation
import UIKit

class SignatureLines{
    var start: CGPoint
    var end: CGPoint
    
    init(start _start:CGPoint, end _end: CGPoint){
        start = _start
        end = _end
    }
}
