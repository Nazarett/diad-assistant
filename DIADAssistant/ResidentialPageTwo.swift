//
//  ResidentialPageTwo.swift
//  DIADAssistant
//
//  Created by Gerardo Nazarett on 10/29/18.
//  Copyright © 2018 Gerardo Nazarett. All rights reserved.
//

import UIKit
import AVFoundation


class ResidentialPageTwo: UIViewController, AVCaptureMetadataOutputObjectsDelegate{
    
    var video = AVCaptureVideoPreviewLayer()
    
    var qrAddress = StreetAddress(stNumber: "", stName: "", stDirection: "", stCity: "", stZip: "", stRoom: "", stFloor: "")
    
    
    
    
    override func viewDidLoad() {
        
        
        let session = AVCaptureSession()
        
        let captureDevice = AVCaptureDevice.default(for: .video)
        
        do{
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            session.addInput(input)
        }catch{
            print("Scan Failed")
        }
        
        let output = AVCaptureMetadataOutput()
        session.addOutput(output)
        
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        
        output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        video = AVCaptureVideoPreviewLayer(session: session)
        
        video.frame = view.layer.bounds
        view.layer.addSublayer(video)
        
        
        session.startRunning()
    }
    
    
    
    
    
    
    //
    //    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects:[Any]!, from connection: AVCaptureConnection!){
    //        if metadataObjects.count > 0 {
    //
    //            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject{
    //                                if object.type == AVMetadataObject.ObjectType.qr{
    //                                    let alert = UIAlertController(title: "Package Detected", message: object.stringValue, preferredStyle: .alert)
    //                                    let action = UIAlertAction(title: "Retake", style: .default, handler: nil)
    //                                    //COME BACK AND ADD HANDLER
    //                                    let actionTwo = UIAlertAction(title: "Continue", style: .default, handler: { (action) in
    //                                        self.performSegue(withIdentifier: "toNextPage", sender: self)
    //                                    })
    //
    //                                    alert.addAction(action)
    //                                    alert.addAction(actionTwo)
    //
    //                                    present(alert, animated: true, completion: nil)
    //                                }
    //                            }
    //            }
    //        }
    //
    //
    
    
    ///NEED TO MAKE QR CODE WITH TRACKING INFORMATION AND ADDRESS
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects != nil && metadataObjects.count != 0 {
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject{
                if object.type == AVMetadataObject.ObjectType.qr{
                    var dataString = object.stringValue!
                    var stNumber = ""
                    var stName = ""
                    var stDr = ""
                    var stCity = ""
                    var stZip = ""
                    var stRoom = ""
                    var stFloor = ""
                    //dataString.firstIndex(of: "|")
                    let start : String.Index = dataString.startIndex
                    var end : String.Index = dataString.firstIndex(of: "|")!
                    stNumber = String(dataString[start..<end])
                    
                    dataString.removeSubrange(start...end)
                    end = dataString.firstIndex(of: "|")!
                    stName = String(dataString[start..<end])
                    
                    dataString.removeSubrange(start...end)
                    end = dataString.firstIndex(of: "|")!
                    stDr = String(dataString[start..<end])
                    
                    dataString.removeSubrange(start...end)
                    end = dataString.firstIndex(of: "|")!
                    stCity = String(dataString[start..<end])
                    
                    dataString.removeSubrange(start...end)
                    end = dataString.firstIndex(of: "|")!
                    stZip = String(dataString[start..<end])
                    
                    dataString.removeSubrange(start...end)
                    end = dataString.firstIndex(of: "|")!
                    stRoom = String(dataString[start..<end])
                    
                    dataString.removeSubrange(start...end)
                    //end = dataString.endIndex
                    //stFloor = String(dataString[start...end])
                    stFloor = dataString
                    
                    qrAddress = StreetAddress(stNumber: stNumber, stName: stName, stDirection: stDr, stCity: stCity, stZip: stZip, stRoom: stRoom, stFloor: stFloor)
                    let finalString = stNumber + " " + stName + " " + stDr + "\n" + stCity + ", " + stZip + "\nRoom: " + stRoom + " Floor: " + stFloor
                    
                    
                    
                    
                    
                    
                    
                    let alert = UIAlertController(title: "Package Detected", message: finalString, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Retake", style: .default, handler: nil)
                    //COME BACK AND ADD HANDLER
                    let actionTwo = UIAlertAction(title: "Continue", style: .default, handler: { (action) in
                        self.performSegue(withIdentifier: "apartment", sender: nil)
                    })
                    
                    alert.addAction(action)
                    alert.addAction(actionTwo)
                    
                    present(alert, animated: true, completion: nil)
                }
            }
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "apartment"{
            if let destination = segue.destination as? CommercialPageTwo{
                destination.qrInfo = qrAddress
            }
            
        }
    }
    
}




