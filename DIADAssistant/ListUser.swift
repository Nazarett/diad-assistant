//
//  ListUser.swift
//  DIADAssistant
//
//  Created by Gerardo Nazarett on 10/15/18.
//  Copyright © 2018 Gerardo Nazarett. All rights reserved.
//

import Foundation

class ListUser{
    let name : String
    let doh : String
    let uid : String
    
    init(name : String, doh : String, uid: String){
        self.name = name
        self.doh = doh
        self.uid = uid
    }
}
