//
//  ForgotPassword.swift
//  DIADAssistant
//
//  Created by Gerardo Nazarett on 11/9/18.
//  Copyright © 2018 Gerardo Nazarett. All rights reserved.
//

import UIKit
import Firebase

class ForgotPassword: UIViewController {
    
    let backgroundImage = UIImageView()
    
    
    @IBOutlet weak var email: UITextField!
    
    
    
    @IBAction func forgotPassword(_ sender: Any) {
        
        Auth.auth().sendPasswordReset(withEmail: email.text!) { (error) in
            if let error = error{
                print(error)
            }else{
                let alert = UIAlertController(title: "Recovery Email Sent", message: "Please check your email to reset your password", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    self.navigationController?.popToRootViewController(animated: true)
                    
                })
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackground()
        
        // Do any additional setup after loading the view.
    }
    
    func setBackground(){
        view.addSubview(backgroundImage)
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        backgroundImage.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImage.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundImage.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundImage.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        backgroundImage.image = UIImage(named: "forgotPassBackground")
        view.sendSubviewToBack(backgroundImage)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
